var gulp        = require('gulp');
var sass        = require('gulp-sass');
var concat      = require('gulp-concat');
var cssmin      = require('gulp-cssmin');
var jsmin       = require('gulp-jsmin');
var browserSync = require('browser-sync').create();

// Compile SASS files on the folder css/scss and concat for and save on css/style.css
gulp.task('sass', function(){
    return gulp.src([
        './vendor/fontawesome-5.6.3/css/all.css',
        './assets/scss/style.scss'])
        .pipe(sass())
        .pipe(concat('style.min.css'))
        .pipe(cssmin())
        .pipe(gulp.dest('./'))
        .pipe(browserSync.stream());
});

// Compile JS files on the folder /assets/js* and concat and save on /assets/js
gulp.task('javascript', function() {
    return gulp.src([
        './assets/js/main.js'])
        .pipe(concat('all.min.js'))
        .pipe(jsmin())
        .pipe(gulp.dest('./assets/js'))
        .pipe(browserSync.stream());
});

// Initialize the project on server proxy for watching changes and execute automatic reloading
gulp.task('serve', function() {
    browserSync.init({
        proxy: "http://localhost/teste-front-end/"
    });
    gulp.watch('./*.html', browserSync.reload);
    gulp.watch('./assets/scss/*.scss', ['sass'], browserSync.reload);
    gulp.watch('./assets/js/*.js', ['javascript'], browserSync.reload);
});

gulp.task('default', ['sass', 'javascript', 'serve']);