// Navbar
menuBar = document.querySelector('.menu_mobile i');
closeMenuBar = document.querySelector('.menu_mobile .close');
clickMenu = document.querySelectorAll('.menu-mobile_items a');

menuBar.addEventListener('click', ()=>{
    toggleMenuMobile();
});
closeMenuBar.addEventListener('click', ()=>{
    toggleMenuMobile();
});

clickMenu.forEach(item => {
  item.addEventListener('click', ()=>{
    toggleMenuMobile();
    smoothScroll();
  });
});

function toggleMenuMobile() {
    document.querySelector('.menu-mobile_items').classList.toggle('d-block');
}

// Smooth scroll
function smoothScroll() {
  const menuItems = document.querySelectorAll('.menu a[href^="#"]');

  menuItems.forEach(item => {
    item.addEventListener('click', scrollToIdOnClick);
  })

  // function scrollSmooth(event) {
  //   event.preventDefault();
  //   const element = event.target;
  //   const id = element.getAttribute('href');
  //   const section = document.querySelector(id).offsetTop;
    
  //   window.scroll({
  //     top: section - 85,
  //     behavior: "smooth",
  //   });
  // }

  function getScrollTopByHref(element) {
    const id = element.getAttribute('href');
    return document.querySelector(id).offsetTop;
  }

  function scrollToIdOnClick(event) {
    event.preventDefault();
    const to = getScrollTopByHref(event.target) - 0;
    scrollToPosition(to);
  }

  function scrollToPosition(to) {
    // Caso queira o nativo apenas
    // window.scroll({
    // top: to,
    // behavior: "smooth",
    // })
    smoothScrollTo(0, to);
  }

  function scrollToIdOnClick(event) {
    event.preventDefault();
    const to = getScrollTopByHref(event.currentTarget) - 0;
    scrollToPosition(to);
  }

  menuItems.forEach(item => {
    item.addEventListener('click', scrollToIdOnClick);
  });

  /**
   * Smooth scroll animation
   * @param {int} endX: destination x coordinate
   * @param {int) endY: destination y coordinate
   * @param {int} duration: animation duration in ms
   */
  function smoothScrollTo(endX, endY, duration) {
    const startX = window.scrollX || window.pageXOffset;
    const startY = window.scrollY || window.pageYOffset;
    const distanceX = endX - startX;
    const distanceY = endY - startY;
    const startTime = new Date().getTime();

    duration = typeof duration !== 'undefined' ? duration : 400;

    // Easing function
    const easeInOutQuart = (time, from, distance, duration) => {
      if ((time /= duration / 2) < 1) return distance / 2 * time * time * time * time + from;
      return -distance / 2 * ((time -= 2) * time * time * time - 2) + from;
    };

    const timer = setInterval(() => {
      const time = new Date().getTime() - startTime;
      const newX = easeInOutQuart(time, startX, distanceX, duration);
      const newY = easeInOutQuart(time, startY, distanceY, duration);
      if (time >= duration) {
        clearInterval(timer);
      }
      window.scroll(newX, newY);
    }, 1000 / 60); // 60 fps
  };
}
smoothScroll();